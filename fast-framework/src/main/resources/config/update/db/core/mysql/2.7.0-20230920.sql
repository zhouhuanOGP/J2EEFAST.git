ALTER TABLE sys_dict_type ADD COLUMN `app_no` varchar(50) NULL DEFAULT 'default' COMMENT '应用编码' AFTER `dict_type`;
ALTER TABLE sys_dict_type ADD COLUMN `app_name` varchar(255) NULL DEFAULT '本系统' COMMENT '应用名称' AFTER `app_no`;
ALTER TABLE sys_dict_data ADD COLUMN `app_no` varchar(50) NULL DEFAULT 'default' COMMENT '应用编码' AFTER `id`;