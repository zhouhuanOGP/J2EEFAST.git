package com.j2eefast.common.event;

import com.j2eefast.common.core.enums.OperationEventType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @author huanzhou
 */
@Getter
@Setter
public class SysDictEvent extends ApplicationEvent {

    private String dictType;
    private String appNo;
    private OperationEventType operationEventType;

    public SysDictEvent(Object source) {
        super(source);
    }
    public SysDictEvent() {
        super(new Object());
    }
}
