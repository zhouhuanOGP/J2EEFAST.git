/*
 * All content copyright http://www.j2eefast.com, unless 
 * otherwise indicated. All rights reserved.
 * No deletion without permission
 */
package com.j2eefast.common.core.config;

import cn.hutool.core.util.StrUtil;
import com.alibaba.druid.pool.DruidDataSource;
import com.j2eefast.common.core.config.properties.DruidProperties;
import com.j2eefast.common.core.io.PropertiesUtils;
import com.j2eefast.common.core.mutidatasource.annotaion.aop.MultiSourceAop;
import com.j2eefast.common.core.utils.ToolUtil;
import com.j2eefast.common.db.context.DataSourceContext;
import com.j2eefast.common.db.factory.AtomikosFactory;
import org.apache.shardingsphere.api.config.masterslave.LoadBalanceStrategyConfiguration;
import org.apache.shardingsphere.api.config.masterslave.MasterSlaveRuleConfiguration;
import org.apache.shardingsphere.shardingjdbc.api.MasterSlaveDataSourceFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.*;

/**
 * <p>基于数据库多数据源配置</p>
 *
 * @author: zhouzhou
 * @date: 2020-04-15 13:52
 * @web: http://www.j2eefast.com
 * @version: 1.0.1
 */
@Configuration
public class DataSourceConfig {

	@Value("#{ @environment['fast.jta.enabled'] ?: false }")
	private boolean enabled;
	private final static String ROUND_ROBIN = "ROUND_ROBIN";
	private final static String WEIGHT = "WEIGHT";

	// ------------------- 读写分离配置 ------ //
	@Value("#{ @environment['spring.datasource.slaveDb.names'] ?: null }")
	private String slaveNames;
	@Value("#{ @environment['spring.datasource.slaveDb.sqlShow'] ?: null }")
	private String sqlshow;
	@Value("#{ @environment['spring.datasource.slaveDb.loadBalanceAlgorithmType'] ?: null }")
	private String loadBalanceAlgorithmType;
	// ----------------------------------------//

	/**
	 * 默认数据库配置
	 */
	@Bean
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource.master")
    public DruidProperties defaultProperties() {
        return new DruidProperties();
    }


	/**
	 * 主数据源实例
	 */
	@Primary
	@Bean("dataSourcePrimary")
	public DataSource dataSourcePrimary(@Qualifier("defaultProperties") DruidProperties druidProperties) throws SQLException {
		if(enabled){
			return AtomikosFactory.create(DataSourceContext.MASTER_DATASOURCE_NAME, druidProperties);
		}else {

			DruidDataSource dataSource0 = new DruidDataSource();
			druidProperties.config(dataSource0);

			if(ToolUtil.isNotEmpty(slaveNames)){
				// 配置真实数据源
				Map<String, DataSource> dataSourceMap = new HashMap<>();
				dataSourceMap.put("MASTER",dataSource0);
				List<String> slaveDataSourceNames = StrUtil.splitTrim(slaveNames, StrUtil.C_COMMA);
//				Properties algorithmProps = new Properties();
//				String loadBalancerName = ToolUtil.isEmpty(loadBalanceAlgorithmType)? ROUND_ROBIN: loadBalanceAlgorithmType;
				slaveDataSourceNames.forEach(e->{
					DruidDataSource slavedataSource = new DruidDataSource();
					DruidProperties tempDruidProperties = new DruidProperties();
					tempDruidProperties.setUsername(PropertiesUtils.getInstance().getProperty("spring.datasource."+e+".username"));
					tempDruidProperties.setUrl(PropertiesUtils.getInstance().getProperty("spring.datasource."+e+".url"));
					tempDruidProperties.setDriverClassName(PropertiesUtils.getInstance().getProperty("spring.datasource."+e+".driverClassName"));
					tempDruidProperties.setPassword(PropertiesUtils.getInstance().getProperty("spring.datasource."+e+".password"));
					tempDruidProperties.setFilters(PropertiesUtils.getInstance().getProperty("spring.datasource."+e+".filters"));
					tempDruidProperties.setDataSourceName(e);
					tempDruidProperties.config(slavedataSource);
//					if(loadBalancerName.equals(WEIGHT)){
//						algorithmProps.put(e,PropertiesUtils.getInstance().getProperty("spring.datasource."+e+".weight"));
//					}
					dataSourceMap.put(e,slavedataSource);
				});

				Properties prop = new Properties();
				prop.put("sql-show",StrUtil.nullToDefault(sqlshow,String.valueOf(Boolean.FALSE)));

				// 配置读写分离规则
				MasterSlaveRuleConfiguration masterSlaveRuleConfig = new MasterSlaveRuleConfiguration("DB_MASTER", "MASTER", slaveDataSourceNames,
						ToolUtil.isEmpty(loadBalanceAlgorithmType)? null: new LoadBalanceStrategyConfiguration(loadBalanceAlgorithmType));
				// 获取数据源对象
				return MasterSlaveDataSourceFactory.createDataSource(dataSourceMap, masterSlaveRuleConfig, prop);
//				ReadwriteSplittingDataSourceRuleConfiguration dataSourceConfig = new ReadwriteSplittingDataSourceRuleConfiguration(
//						"READ_QUERY_DS", "MASTER",
//						dataSourceMap.keySet().stream().collect(Collectors.toList()),
//						ToolUtil.isEmpty(loadBalanceAlgorithmType)? "ROUND_ROBIN": loadBalanceAlgorithmType);
//				Map<String, AlgorithmConfiguration> algorithmConfigMap = new HashMap<>(1);
//				algorithmConfigMap.put(ToolUtil.isEmpty(loadBalanceAlgorithmType)? ROUND_ROBIN: loadBalanceAlgorithmType,
//						new AlgorithmConfiguration(ToolUtil.isEmpty(loadBalanceAlgorithmType)? ROUND_ROBIN: loadBalanceAlgorithmType, algorithmProps));
//				ReadwriteSplittingRuleConfiguration ruleConfig = new ReadwriteSplittingRuleConfiguration(Collections.singleton(dataSourceConfig), algorithmConfigMap);
//
//
//				return ShardingSphereDataSourceFactory.createDataSource(
//						"MASTER",new ModeConfiguration("Standalone", new StandalonePersistRepositoryConfiguration("JDBC", new Properties()))
//						,dataSourceMap,Collections.singleton(ruleConfig)
//						, prop);

			}
			return dataSource0;
		}
	}

	/**
	 * 多数据源切换的aop
	 */
	@Bean
	public MultiSourceAop multiSourceExAop() {
		return new MultiSourceAop();
	}

}
